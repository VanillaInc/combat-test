﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

    // Components
    [System.NonSerialized]
    public Rigidbody2D rb;

    // Stats
    public float moveSpeed;

    // GameObjects
    private GameObject player;

    // Scripts
    private PlayerAttack pa;

    // Animation Bools
    private bool playerMoving;

    //[System.NonSerialized]
    public int facing = 1;
    // Facing left = -1; right = 1;

    // Input
    private float horiz;
    [System.NonSerialized]
    public float vert;

	// Use this for initialization
	void Start () {
        player = transform.GetChild(0).gameObject;

        rb = GetComponent<Rigidbody2D>();
        pa = player.GetComponent<PlayerAttack>();
    }

    // Update is called once per frame
    void Update() {

        horiz = Input.GetAxisRaw("Horizontal");
        vert = Input.GetAxisRaw("Vertical");
        // } Input Vars

        // Movement {
        if (pa.askForInput)
        {
            rb.velocity = new Vector2(horiz * moveSpeed, rb.velocity.y);

            playerMoving = rb.velocity != Vector2.zero;
        }
        // } Movement

        // Left
        if (horiz < -0.5)
        {
            facing = -1;
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (horiz > 0.5)
        {
            facing = 1;
            transform.localScale = new Vector3(1, 1, 1);
        }

        transform.position = player.transform.position;
    }
}
