﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

    //Components
    private playerController player;
    private Animator anim;

    // Attack
    public bool attacking;
    public bool askForInput = true;

    private bool attackPressed;

    private int comboIndex;

    // 0 Ground; 1 Air; 2 Low;
    public AttackAtt[] attackAtts;

    // Start of scene
    void Start() {
        anim = GetComponent<Animator>();
        player = transform.parent.GetComponent<playerController>();

        //I don't want this shit at the top
        Start2();
    }

    // Every frame
    void Update() {
        attackPressed = Input.GetKeyDown(KeyCode.X);

        if (attackPressed && askForInput)
            AttackPreface();
    }

    // What do you think this does
    private void AttackPreface()
    {
        attacking = true;

        if(player.vert == 0)
        {
            Attack(attackAtts[0].forces, attackAtts[0].triggers);
        }
        if(player.vert > 0.5)
        {
            Attack(attackAtts[1].forces, attackAtts[1].triggers);
        }
        if(player.vert < -0.5)
        {
            Attack(attackAtts[2].forces, attackAtts[2].triggers);
        }
    }

    private void Attack(Vector2[] attackForces, string[] triggers)
    {
        player.rb.velocity = Vector2.zero;
        askForInput = false;

        SwitchTrigger(triggers);

        // Facing reverses the value if left
        player.rb.AddForce(new Vector2(attackForces[comboIndex].x * player.facing * 100, attackForces[comboIndex].y * 100));

        comboIndex++;
    }

    void SwitchTrigger(string[] triggers)
    {
        switch (comboIndex)
        {
            case 0:
                anim.SetTrigger(triggers[0]);
                break;
            case 1:
                anim.SetTrigger(triggers[1]);
                break;
            case 2:
                anim.SetTrigger(triggers[2]);
                break;
        }
    }

    void Start2()
    {
        
    }
}

[System.Serializable]
public class AttackAtt
{
    public string name;
    public Vector2[] forces;
    public string[] triggers;
}